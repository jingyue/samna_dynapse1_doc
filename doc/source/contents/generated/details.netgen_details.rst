﻿details.netgen\_details
=======================

.. automodule:: details.netgen_details

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      check_and_write_post_cam
      check_and_write_pre_sram
      convert_validated_network2dynapse1_configuration
      find_neuron_in_dict
      find_pre_in_post_incoming
      gen_all2all_lists
      gen_neuron_string
      gen_one2one_lists
      get_usable_post_synapse_id
      get_usable_pre_destination_id
      is_same_neuron
      validate
      weight_matrix2lists
      write_post_synapse
      write_pre_destination
   
   

   
   
   

   
   
   



