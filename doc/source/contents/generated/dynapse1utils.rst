﻿dynapse1utils
=============

.. automodule:: dynapse1utils

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      close_dynapse1
      create_neuron_select_graph
      free_port
      gen_destination_string
      gen_synapse_string
      get_Dynapse1_viz_converter
      get_global_id
      get_global_id_list
      get_neuron_from_config
      get_parameter_value
      get_parameters
      get_selected_timestamps
      get_selected_traces
      get_serial_number
      get_time_wrap_events
      get_trace_value
      open_device
      open_dynapse1
      open_gui
      open_visualizer
      print_dynapse1_spike
      print_neuron_destinations
      print_neuron_synapses
      save_parameters2json_file
      save_parameters2txt_file
      set_fpga_spike_gen
      set_parameters_in_json_file
      set_parameters_in_txt_file
   
   

   
   
   

   
   
   



