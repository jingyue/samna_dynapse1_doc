﻿netgen
======

.. automodule:: netgen

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      add_synapses
      add_wta_conns
      print_post_neuron_dict
      remove_synapses
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Network
      NetworkGenerator
      Neuron
      NeuronGroup
      Synapses
      WTA_connections
   
   

   
   
   



