﻿params
======

.. automodule:: params

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      gen_clean_param_group
      gen_dc_params
      gen_param_group
      gen_stdp_params
      set_params
      set_stdp_params
   
   

   
   
   

   
   
   



